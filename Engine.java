package carExample;

public class Engine {
    private String power;
    private String producer;

    public Engine(String power, String producer) {
        this.power = power;
        this.producer = producer;
    }

    public Engine() {

    }

    public String getPower() {
        return this.power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getProducer() {
        return this.producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "power='" + power + '\'' +
                ", producer='" + producer + '\'' +
                '}';
    }
}
