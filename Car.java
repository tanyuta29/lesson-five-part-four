package carExample;

public class Car {
    private String producer;
    private String aClass;
    private double weight;
    private Driver driver;
    private Engine engine;

    private int drivingMode;


    public int getDrivingMode() {
        return this.drivingMode;
    }

    public void setDrivingMode(int drivingMode) {
        this.drivingMode = drivingMode;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getProducer() {
        return this.producer;
    }


    public String getaClass() {
        return this.aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return this.driver;
    }

    public String setDriver(String driver) {
        return driver;
    }

    public Engine getEngine() {
        return this.engine;
    }

    public void setEngine(String engine) {
        engine = engine;
    }

    public void start() {
        System.out.println("Поехали");
    }

    public void stop() {
        System.out.println("Останавливаемся");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот налево");
    }

    public void openTrunk() {
        System.out.println("Відкрити багажник");
    }

    public void closeTrunk() {
        System.out.println("Закрити багажник");
    }

    public void turnOnHeadlight() {
        System.out.println("Включити фари");
    }

    public void turnOffHeadlight() {
        System.out.println("Виключити фари");
    }

    public void turnSignal() {
        System.out.println("Включення сигналу");
    }

    public void turnOnBacklight() {
        System.out.println("Включити підсвітку");
    }

    public void turnOffBacklight() {
        System.out.println("Виключити відсвітку");
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", aClass='" + aClass + '\'' +
                ", weight=" + weight +
                ", driver=" + driver +
                ", engine=" + engine +
                '}';
    }


}

