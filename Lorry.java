package carExample;

public class Lorry extends Car {
    private double liftingCapacity;


    public double getLiftingCapacity() {
        return this.liftingCapacity;
    }

    public void setLiftingCapacity(double liftingCapacity) {
        this.liftingCapacity = liftingCapacity;
    }

    @Override
    public String setDriver(String driver) {
        super.setDriver(driver);
        return driver;
    }

    @Override
    public void setEngine(String engine) {
        super.setEngine(engine);
    }

    @Override
    public String toString() {
        return "Lorry{" +
                "liftingCapacity=" + liftingCapacity +
                "} " + super.toString();
    }

}

