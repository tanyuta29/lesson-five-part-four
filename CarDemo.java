package carExample;

public class CarDemo extends Car {
    public static void main(String[] args) {

        Driver bmwDriver = new Driver("Иванов В.В.", 50, 30);
        Engine bmwEngine = new Engine("60", "BMW");
        Car car = new Car();
        car.setProducer("BMW");
        car.setaClass("C");
        car.setWeight(5000);
        car.setDriver("bmwDriver");
        car.setEngine("bmwEngine");

        Driver lorryDriver = new Driver("Петров В.В.", 45, 20);
        Engine lorryEngine = new Engine("30", "LorryEngine");

        Lorry lorry = new Lorry();
        lorry.setProducer("Грузовик");
        lorry.setaClass("D");
        lorry.setWeight(8000);
        lorry.setDriver("lorryDriver");
        lorry.setEngine("lorryEngine");
        lorry.setLiftingCapacity(70);

        Class myClass = lorry.getClass();
        Driver sportDriver = new Driver("Сидоров В.В.", 30, 15);
        Engine sportEngine = new Engine("80", "SportEngine");

        SportCar sportCar = new SportCar();
        sportCar.setProducer("SportCar");
        sportCar.setaClass("C");
        sportCar.setWeight(4000);
        sportCar.setDriver("sportDriver");
        sportCar.setEngine("sportEngine");
        sportCar.setSpeed(300);

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);
    }
}
